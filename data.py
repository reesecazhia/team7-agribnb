DATA = {
    "rice": {
        "title": "[ESPECIAL EVENT] Rice Planting with Alden Richards",
        "tags": ["planting", "cultivation", "rice"],
        "info": {
            "location": "Tayabas Quezon",
            "lat": 14.0286617,
            "lng": 121.5832461,
            "duration": "9 Hours",
            "food_provision": "Breakfast and Lunch",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in a paid activity!"
            },
        ],

        "images": [
            "rice2.jpg",
            "rice1.jpg",
            "rice3.jpg",
            "rice4.jpg",
            "rice5.jpg"
        ],

        "what_to_do": "<p>Rice has been a staple of the Filipino diet for a while now.  It is consumed in each and every cultural region of this diverse nation. However, the process of planting rice is still unknown to many.</p> <p> In this activity, everyone is encouraged to come along in the adventure of planting rice together with a Filipino actor: Alden Richards! You get to have an amazing time while learning farming.</p> <p>Bring your friends, family, kids or parents along for this unique experience!",
        "farmer": {
            "name": "Lando",
            "full_name": "Lando Perez",
            "designation": "Rice Farmer",
            "image": "farm_person.jpg",
            "desc": "<p></p>            <p> Mang Lando had been a farmer for almost 45 years now. Rice planting is his main focus and his main source of income. Over the years, he discovered different techniques on farming that helps his job.</p> <p> His family were able to learn farming from him including his young children. Since he is growing older, he wishes to help the youth understand and learn the importance of rice planting."
        },

        "activity": [
            {
                "time": "6:00-7:00",
                "title": "Tour and Breakfast",
                "detail": "A tour of the whole farm and a local breakfast."
            },
            {
                "time": "7:00-9:00",
                "title": "Tutorial on how to do Rice Planting",
                "detail": "Discussions about the steps of rice planting."
            },
            {
                "time": "9:00-12:00",
                "title": "Planting Rice with Alden Richards",
                "detail": "A whole experience of planting rice on the field with a Filipino artist- Alden Richards! "
            },
            {
                "time": "12:00-1:00",
                "title": "Lunch",
                "detail": "We'll eat lunch!"
            },
            {
                "time": "1:00-2:00",
                "title": "Seminars and Additional Techniques in Planting Rice",
                "detail": "Discussion of further techniques and encourangement of committing to rice planting."
            }
        ],

        "about_place": "<p>Tayabas, officially the City of Tayabas, (Tagalog: Lungsod ng Tayabas), or simply known as Tayabas City, is a 6th class city in the province of Quezon, Philippines.  The major agricultural products of Tayabas are rice and coconut. It is also known for Sweet delicacies, Budin (Cassava cake) and lambanog. It is known for lambanog (coconut arrack) and sweet food/delicacies, as well as tourism resorts. Tayabas is also known as the City of Festivals because of its colorful festivals. The city is known for resorts, heritage houses, historical landmarks, more than 20 Spanish stone bridges with under-carvings from Filipino ancestors.</p><p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Sat, Aug 31 - 6:00 AM to 2:00 PM",
                "price": 100,
                "slots": "4 of 40"
            },
            {
                "date": "Sat, Sept 1 - 6:00 AM to 2:00 PM",
                "price": 100,
                "slots": "23 of 40"
            },
            {
                "date": "Sat, Sept 2 - 6:00 AM to 2:00 PM",
                "price": 100,
                "slots": "40 of 40"
            }
        ]
    },

    "grazing": {
        "title": "Enjoy an Afternoon Carabao Grazing in the Mountains",
        "tags": ["carabao", "farm", "grazing"],
        "info": {
            "location": "Lucban Quezon",
            "lat": 14.133807,
            "lng": 121.559303,
            "duration": "5 Hours",
            "food_provision": "Snacks and Dinner",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in a paid activity!"
            },
        ],

        "images": [
            "carabao2.jpg",
            "carabao1.jpg",
            "carabao3.png",
            "carabao5.jpg",
            "carabao4.jpg"
        ],

        "what_to_do": "<p>Challenge yourself by going hiking on a mountain while grazing carabaos! This activity promotes quality time with your love ones while enjoying a challenging task. Enjoy a relaxing sunset view while descending the mountain and have dinner at a ranch while resting.</p>",
        "farmer": {
            "name": "Kulas",
            "full_name": "Nicolo Sumilang",
            "designation": "Rancher",
            "image": "carabao-farmer.jpg",
            "desc": "<p></p>            <p>Mang Kulas is a rancher which started when he acquired a land in the province. He had been taking care of these animals for almost 7 years now and he makes profit from them. He uses these animals for farming.</p> <p>Mang Kulas teaches others about farm animal grazing to educate others on the fun of the activity! He also allows customers to take his animals up the Mountain for grazing.</p>"
        },

        "activity": [
            {
                "time": "2:00-3:00",
                "title": "Ranch Tour",
                "detail": "We'll go around the ranch and see farm animals."
            },
            {
                "time": "3:00-5:00",
                "title": "Travelling up the Mountain while Grazing Carabaos",
                "detail": "We'll go hiking with the carabaos while they eat!"
            },
            {
                "time": "5:00-6:00",
                "title": "Descending the Mountain",
                "detail": "We'll go down the mountain while enjoying the sunset view."
            },
            {
                "time": "6:00-7:00",
                "title": "Dinner",
                "detail": "We'll eat dinner while enjoying a cozy bonfire!"
            },

        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Wed, Sept 11 - 2:00 PM to 7:00 PM",
                "price": 150,
                "slots": "8 of 15"
            },
            {
                "date": "Fri, Sept 13 - 2:00 PM to 7:00 PM",
                "price": 150,
                "slots": "8 of 15"
            },
            {
                "date": "Sun, Sept 15 - 2:00 PM to 7:00 PM",
                "price": 150,
                "slots": "8 of 15"
            }
        ]

    },

    "cow_milk": {
        "title": "Fun Cheese Making Lesson from Freshly Collected Cow Milk",
        "tags": ["milking", "cow", "cheesemaking"],
        "info": {
            "location": "Calamba Laguna",
            "lat": 14.188831,
            "lng": 121.126191,
            "duration": "7 Hours and 30 minutes",
            "food_provision": "Breakfast, Snack and Lunch",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "cow1.jpg",
            "cow2.jpg",
            "cow3.jpg",
            "cow4.jpg",
            "cow5.jpg"
        ],

        "what_to_do": "<p>This event encourage you to experience milking a cow and try to make your very own cheese. You will feel the excitement and it's a new way of spending free time. </p>",
        "farmer": {
            "name": "Juan",
            "full_name": "Juan Bagatsing",
            "designation": "Livestock Dealer",
            "image": "cowfarmer.jpg",
            "desc": "<p></p>            <p>Mang Juan is a livestock dealer for 15 years now on the Province of Sariaya. He is a milk and cheese producer from his own cows. His whole family is part of the business and it is their main source of income.</p><p>He is well known in the province for being hardworking. He offers services on teaching people cow milking and cheese making.</p>"
        },
        "activity": [
            {
                "time": "7:00-8:00",
                "title": "Barnyard Tour and Breakfast",
                "detail": "A full tour of the barnyard and a breakfast."
            },
            {
                "time": "8:00-8:30",
                "title": "Instructions on Cow Milking",
                "detail": "Instructions on how to do cow milking will be given."
            },
            {
                "time": "8:30-11:00",
                "title": "Cow Milking",
                "detail": "We'll be doing a cow milking session!"
            },
            {
                "time": "11:00-12:00",
                "title": "Lunch",
                "detail": "We'll eat lunch!"
            },
            {
                "time": "12:00-12:30",
                "title": "Instructionson Cheese  Making",
                "detail": "Instructions on how to do cheese making will be given."
            },
            {
                "time": "12:30-2:00",
                "title": "Cheese Making",
                "detail": "We'll be doing a cheese making session!"
            },
            {
                "time": "2:00-2:30",
                "title": "Snacks",
                "detail": "We'll have snacks!"
            },

        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Sat, Sept. 7 - 7:00 AM to 2:30 PM",
                "price": 200,
                "slots": "23 of 45"
            },
            {
                "date": "Sat, Sept. 14 - 7:00 AM to 2:30 PM",
                "price": 200,
                "slots": "18 of 45"
            },
            {
                "date": "Sat, Sept. 21 - 7:00 AM to 2:30 PM",
                "price": 200,
                "slots": "26 of 45"
            }
        ]

    },

    "honey": {
        "title": "Exciting Honey Gathering - Wear Real Bee Suites!",
        "tags": ["gathering", "honey", "bee"],
        "info": {
            "location": "Lipa, Batangas",
            "lat": 13.939043,
            "lng": 121.169644,
            "duration": "5 Hours",
            "food_provision": "Lunch and Snacks",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "tomato1.jpeg",
            "tomato2.jpeg",
            "tomato3.jpg",
            "tomato4.jpg",
            "tomato5.jpg"
        ],

        "what_to_do": "<p>People want something new, and extracting honey might caught their attention. The protective gear will be provided by the staffs. This activity offers you the best moment you'll experience in extracting honeys. </p>",
        "farmer": {
            "name": "Toni",
            "full_name": "Antonio Dimagiba",
            "designation": "Beekeeper",
            "image": "honeykeeper.jpg",
            "desc": "<p></p>            <p>Mang toni is a bee keeper, he is an expert in extracting honey. His inspiration is his family because its their family business. He enjoys the life in farm where the air is clean and fresh.</p>"
        },

        "activity": [
            {
                "time": "10:00-11:30",
                "title": "Orientation",
                "detail": "We'll go through some screening and checking each parcipant's health"
            },
            {
                "time": "11:30-12:00",
                "title": "Lunch and Demonstration",
                "detail": "We'll eat lunch and enjoy the demonstration of the bee keeper."
            },
            {
                "time": "12:00-2:00",
                "title": "We'll extract honeys!",
                "detail": "We'll go around, collect honeys, and extract it."
            },
            {
                "time": "2:00-3:00",
                "title": "Snacks",
                "detail": "We'll eat snacks and have fun with your honey."
            },

        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Tue, Sept. 3 - 10:00 AM to 3:00 PM",
                "price": 150,
                "slots": "10 of 40"
            },
            {
                "date": "Thu, Sept. 5 - 10:00 AM to 3:00 PM",
                "price": 150,
                "slots": "13 of 40"
            },
            {
                "date": "Sat, Sept. 7 - 10:00 AM to 3:00 PM",
                "price": 150,
                "slots": "26 of 40"
            }
        ]
    },

    "banana": {
        "title": "Banana Cultivation Orientation - We'll also make some traditional Nilupak!",
        "tags": ["planting", "nilupak", "banana"],
        "info": {
            "location": "Mauban Quezon",
            "lat": 14.184012,
            "lng": 121.714577,
            "duration": "9 Hours",
            "food_provision": "Breakfast and Lunch",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "nilupak1.jpg",
            "nilupak2.jpg",
            "nilupak3.jpg",
            "nilupak4.jpg",
            "nilupak5.png"
        ],

        "what_to_do": "<p> </p>",
        "farmer": {
            "name": "Tomas",
            "full_name": "Tomas Aquino",
            "designation": "Crop Farmer",
            "image": "nilupakfarmer.jpg",
            "desc": "<p></p>            <p>Mang Tomas is an ordinary farmer in the province. He gets  to enjoy the urban life with his family while doing farm related job. He particularly plants crops but he also owns a handful of farm animals.</p>"
        },

        "activity": [
            {
                "time": "9:00-10:00",
                "title": "Breakfast and Farm Tour",
                "detail": "We'll eat breakfast and will be guided in the farm."
            },
            {
                "time": "10:00-12:00",
                "title": "Banana Cultivation",
                "detail": "Instructions on how to do banana cultivation will be given."
            },
            {
                "time": "12:00-1:00",
                "title": "Lunch",
                "detail": "We'll eat lunch!"
            },
            {
                "time": "1:00-3:00",
                "title": "Snacks and Traditional Nilupak making",
                "detail": "We're going to make some traditional nilupak for our snacks."
            },
        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Sun, Sept. 8 - 9:00 AM to 3:00 PM",
                "price": 100,
                "slots": "25 of 40"
            },
            {
                "date": "Sun, Sept. 15 - 9:00 AM to 3:00 PM",
                "price": 100,
                "slots": "32 of 40"
            },
            {
                "date": "Sun, Sept. 22 - 9:00 AM to 3:00 PM",
                "price": 100,
                "slots": "35 of 40"
            }
        ]
    },

    "corn": {
        "title": "Corn Harvesting Session - Free corn products!",
        "tags": ["planting", "corn", "products", "free"],
        "info": {
            "location": "Pagsanjan Laguna",
            "lat": 14.262986,
            "lng": 121.461202,
            "duration": "6 Hours",
            "food_provision": "Breakfast and Lunch",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "corn1.jpg",
            "corn2.jpg",
            "corn3.jpg",
            "corn4.jpg",
            "corn5.jpg"
        ],

        "what_to_do": "<p>Corn is a hybridized variety of maize with a high sugar content.  It is picked when immature and prepared and eaten as a vegetable, rather than a grain. In this activity,  everyone is expected to keep an eye to sweet corn to keep it away from insects. You will be taught on what to do and what not to do to a corn plant and eventually learn how to really take care of it. There will be free corn products after the activity.</p>",
        "farmer": {
            "name": "Fernando",
            "full_name": "Fernando Jacinto",
            "designation": "Crop Farmer",
            "image": "cornfarmer.jpg",
            "desc": "<p></p> <p>Mang Fernando is a cornfield owner for about 15 years now. He inherited the field and knowledge of farming from his father. Now, he teaches his children farming and his family started making corn products.</p>"
        },

        "activity": [
            {
                "time": "7:00-9:00",
                "title": "Farm Tour and Breakfast",
                "detail": "A full tour of the farm and breakfast."
            },
            {
                "time": "9:00-9:30",
                "title": "Instructions on Corn Harvesting",
                "detail": "Instructions on how to do corn harvesting will be given."
            },
            {
                "time": "9:30-12:00",
                "title": "Corn Harvesting",
                "detail": "We're going to harvest corn."
            },
            {
                "time": "12:00-1:00",
                "title": "Lunch",
                "detail": "We'll eat lunch!"
            },
            {
                "time": "1:00-2:00",
                "title": "Giving Corn Products",
                "detail": "Corn Products will be given for free "
            },

        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Sun, Sept 1 - 7:00 AM to 2:00 PM",
                "price": 100,
                "slots": "40 of 60"
            },
            {
                "date": "Mon, Sept 2 - 7:00 AM to 2:00 PM",
                "price": 100,
                "slots": "24 of 40"
            },
            {
                "date": "Wed, Sept  - 7:00 AM to 2:00 PM",
                "price": 100,
                "slots": "26 of 40"
            }
        ]

    },

    "sugar_cane": {
        "title": "Sugar Cane MILK TEA making!",
        "tags": ["harvesting", "sugar_cane", "milk_tea"],
        "info": {
            "location": "Liliw, Laguna",
            "lat": 14.137008,
            "lng": 121.442270,
            "duration": "7 Hours and 30 minutes",
            "food_provision": "Breakfast, Lunch and Snacks",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "sugarcane1.jpg",
            "sugarcane2.jpg",
            "sugarcane3.jpg",
            "sugarcane4.jpg",
            "sugarcane5.jpg"
        ],

        "what_to_do": "<p>Sugarcane is an established source of sugar and is the current benchmark first-generation feedstock for efficient biofuel production. In this activity, everyone will understand and learn the proper way on harvesting a sugarcane. After harvesting it, the sweet taste of sugarcane will be extracted and the extracted juice will be used by the participants in making their own milk tea according to what flavor they like. they will be guided by an expert milk tea shop owner invited by the farmer herself. </p> ",
        "farmer": {
            "name": "Puring",
            "full_name": "Pamela Magdalena",
            "designation": "Crop Farmer",
            "image": "sugarcanefarmer.jpg",
            "desc": "<p></p> <p>Aling Puring raised lots of different crops and cared for many varied animals. She owns a hectare of field full of different animals and plants which includes the sugarcane. Harvesting and selling sugarcane is mainly the one that helped her family survived through the years. </p> <p> Her mother taught her farming and now she's the one teaching her children and many teenagers planting and harvesting crops. Now, she continues on finding more uses of the sugarcane plant as a food. </p>"
        },

        "activity": [
            {
                "time": "7:00-9:00",
                "title": "Farm Tour and Breakfast",
                "detail": "A full tour of the farm and breakfast."
            },
            {
                "time": "9:00-9:30",
                "title": "Instructions on Sugarcane Harvesting",
                "detail": "Instructions on how to do sugarcane harvesting will be given."
            },
            {
                "time": "9:30-12:00",
                "title": "Sugarcane Harvesting",
                "detail": "We're going to harvest corn."
            },
            {
                "time": "12:00-1:00",
                "title": "Lunch",
                "detail": "We'll eat lunch!"
            },
            {
                "time": "1:00-1:30",
                "title": "Instructions and Demonstrations on Making Milk Tea.",
                "detail": "Demonstrations on how to make a Milk Tea will be instructed."
            },
            {
                "time": "1:30-2:30",
                "title": "Making Milk Tea and having Snack",
                "detail": "We're going to make milktea together with a snack!"
            },
        ],

        "about_place": "<p>Liliw is perhaps best known for its cold water spring resorts, native homemade sweets and a sizeable shoe industry that rivals that of Marikina City. The town is also known for its baroque church and its Liliw-style houses. The local government is currently undertaking means to conserve its cultural heritage sites and has proposed to enact a legislation that would mandate the usage of the Liliwstyle architecture as the only means of construction and re-construction in the town.</p><p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Sat, Aug 31 - 7:00 AM to 2:30 PM",
                "price": 120,
                "slots": "35 of 55"
            },
            {
                "date": "Tue, Sept 3 - 7:00 AM to 2:30 PM",
                "price": 120,
                "slots": "26 of 55"
            },
            {
                "date": "Fri, Sept 6 - 7:00 AM to 2:30 PM",
                "price": 120,
                "slots": "40 of 55"
            }
        ]

    },

    "coconut": {
        "title": "Enjoy Coconut Harvesting while Riding the 'Parägos'!",
        "tags": ["harvesting", "coconut", "paragos"],
        "info": {
            "location": "Luisiana, Laguna",
            "lat": 14.189481,
            "lng": 121.526903,
            "duration": "5 Hours and 30 minutes",
            "food_provision": "Breakfast and Lunch",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "paragos1.jpg",
            "paragos2.jpg",
            "paragos3.jpg",
            "paragos4.jpg",
            "paragos5.jpg"
        ],

        "what_to_do": "<p> Coconut tree is called the 'Tree of Life' because every part of it have different uses. This tree is is just simple yet can provide as one of the basic needs of people. In this activity, the people will ride a paragos on their way to get coconuts then they will be send back to the farm riding a paragos again.</p> ",
        "farmer": {
            "name": "Cardo",
            "full_name": "Cardomeng Diosmio",
            "designation": "Crop Farmer",
            "image": "paragosfarmer.jpg",
            "desc": "<p></p> <p>Mang Cardo is a farmer and a police for about 3 years now. He grew up in a poor family and dreamed to be a police. But still persued his families tradition of farming. Out of his hardwork, he now owns an entire land filled with coconut trees. </pr> <pr> He is used to riding paragos before when going to school but now he was able to provide his own transportation. He let's people experience a ride with a paragos carried by a farm animal of their choice."
        },

        "activity": [
            {
                "time": "8:00-9:00",
                "title": "Farm Tour and Breakfast",
                "detail": "A full tour of the farm and breakfast."
            },
            {
                "time": "9:00-9:30",
                "title": "Riding a Paragos",
                "detail": "We're going to ride a paragos on the way to the coconut trees"
            },
            {
                "time": "9:30-11:30",
                "title": "Coconut Harvesting",
                "detail": "We're going to harvest coconut."
            },
            {
                "time": "11:30-12:30",
                "title": "Lunch",
                "detail": "We'll eat lunch!"
            },
            {
                "time": "12:30-1:30",
                "title": "Riding a Paragos",
                "detail": "We're going to ride a paragos on the way back to the farm."
            },

        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Sun, Sept 1 - 8:00 AM to 1:30 PM",
                "price": 100,
                "slots": "30 of 60"
            },
            {
                "date": "Tue, Sept 3 - 8:00 AM to 1:30 PM",
                "price": 100,
                "slots": "31 of 60"
            },
            {
                "date": "Sat, Sept 7 - 8:00 AM to 1:30 PM",
                "price": 100,
                "slots": "40 of 60"
            }
        ]

    },
    "river": {
        "title": "Beat the Heat while catching Tilapia at the local River!",
        "tags": ["fishing", "river", "tilapia"],
        "info": {
            "location": "Lucena, Quezon",
            "lat": 13.943712,
            "lng": 121.625743,
            "duration": "9 hours",
            "food_provision": "Breakfast and Lunch",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "fishing1.jpg",
            "fishing2.jpg",
            "fishing3.jpg",
            "fishing4.jpg",
            "fishing5.jpg"
        ],

        "what_to_do": "<p> Too tired of the polluted city air? Go to the province and get a breath of fresh air!</p>  <p>You'll fish and relax at the local river. Enjoy catching tilapia with your good friends while enjoying the nature and ambiance of the place.</p> ",
        "farmer": {
            "name": "Rico",
            "full_name": "Ricochan Domingo",
            "designation": "Fisherman",
            "image": "fishingfisherman.png",
            "desc": "<p></p> <p>Mang Rico is a fisherman who used to live by the seashore. He discovered ways of fresh water fishing and decided to commit on it. He built his own fish pond by the river. Right now, he has a growing business on fresh water fishing.</p>"
        },

        "activity": [
            {
                "time": "8:00-9:00",
                "title": "Breakfast",
                "detail": "We'll eat breakfast."
            },
            {
                "time": "9:00-12:00",
                "title": "Tilapia Fishing",
                "detail": "We'll enjoy fishing tilapia by the river."
            },
            {
                "time": "12:00-1:00",
                "title": "Cooking your Tilapia",
                "detail": "We'll get to cook what we caught for our lunch!"
            },
            {
                "time": "1:00-5:00",
                "title": "Relax by the River",
                "detail": "Enjoy our remaining time by relaxing by the river."
            },

        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Sat, Sept 14 - 8:00 AM to 5:00 PM",
                "price": 100,
                "slots": "17 of 30"
            },
            {
                "date": "Sat, Sept 21 - 8:00 AM to 5:00 PM",
                "price": 100,
                "slots": "21 of 30"
            },
            {
                "date": "Sat, Sept 28 - 8:00 AM to 5:00 PM",
                "price": 100,
                "slots": "12 of 30"
            }
        ]

    },
    "amazing_race": {
        "title": "Amazing Palayan After Harvest Race! - With Pig Catching!",
        "tags": ["pig", "race", "rice", "adventure"],
        "info": {
            "location": "Pagbilao, Quezon",
            "lat": 13.986524,
            "lng": 121.755297,
            "duration": "6 hours",
            "food_provision": "Snacks and Dinner",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "pig-catching.jpg",
            "pig-catching2.jpg",
            "pig-catching3.jpg",
            "pig-catching4.jpeg",
            "pig-catching5.jpg"
        ],

        "what_to_do": "<p>This activity provides a traditional game - pig catching! You'll get to do it messy and carefree on a newly harvested rice field. Enjoy challenges and activities while having fun with your collegues and going back to the traditional way!</p> ",
        "farmer": {
            "name": "Ignacio",
            "full_name": "Ignacio Dimaculangan",
            "designation": "",
            "image": "pig-catchingfarmer.jpg",
            "desc": "<p></p> <p>Mang Ignacio likes watching his grandchildren to have fun. So he deviced this activity with the help of his grandchildren. He had been farming for almost 47 years now. He watched his kids grow up with the rice fields that he owns. Mang Ignacio wishes to see different people have fun in the rice fields.</p>"
        },

        "activity": [
            {
                "time": "2:00-3:00",
                "title": "Snacks",
                "detail": "We'll open our activity with a snack for energy!"
            },
            {
                "time": "3:00-7:00",
                "title": "Amazing Race on Rice Fields",
                "detail": "Enjoy traditional challenges on the rice fields."
            },
            {
                "time": "7:00-8:00",
                "title": "Dinner",
                "detail": "We'll eat dinner while resting from the exhausting activity."
            },
        ],

        "about_place": "<p>Pagbilao, officially the Municipality of Pagbilao, is a 1st class municipality in the province of Quezon, Philippines. Most of the vast lands of Pagbilao are mainly used for agricultural. These agricultural land are mostly coconut, rice and poultry farms. But some of these lands became industrial areas. Most of these industrial lands are ice plants, rice and oil mills which can be seen along the highway.</p><p>Tourism is also growing in Pagbilao. There are many hotel and resorts can be found along the highway or in the beachfront of Barangay Bantigue, which has a great view of the islands of the town.</p><p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Wed, Sept 18 - 2:00 PM to 8:00 PM",
                "price": 300,
                "slots": "32 of 45"
            },
            {
                "date": "Thu, Sept 19 - 2:00 PM to 8:00 PM",
                "price": 300,
                "slots": "28 of 45"
            },
            {
                "date": "Fri, Sept 20 - 2:00 PM to 8:00 PM",
                "price": 300,
                "slots": "21 of 45"
            }
        ]

    },

    "star_gazing": {
        "title": "Catching Crabs Under the Stars",
        "tags": ["stars", "crabs", "night", "gazing"],
        "info": {
            "location": "San Juan, Batangas",
            "lat": 13.750447,
            "lng": 121.373680,
            "duration": "6 hours",
            "food_provision": "Snacks and Dinner",
            "logistics": "Tools Provided",
            "first_aids": "First Aid Kits"
        },
        "badges": [
            {
                "url": "farm_badge.png",
                "description": "Earned for every farm related activity!"
            },
            {
                "url": "green_badge.png",
                "description": "Earned for every planting related events!"
            },
            {
                "url": "friend_badge.png",
                "description": "Earned when participating in an paid activity!"
            },
        ],

        "images": [
            "star.jpg",
            "star1.jpg",
            "star2.jpg",
            "star3.jpg",
            "star4.jpg"
        ],

        "what_to_do": "<p>Catch crabs at night and then enjoy a star gazing adventure by the seashore. You get to  get a close view of the stars using our provided telescope. Enjoy this sweet ambiance with your love ones.</p> ",
        "farmer": {
            "name": "Auring",
            "full_name": "Auring Santos",
            "designation": "Crab Farmer",
            "image": "starfisher.jpg",
            "desc": "<p></p> <p>Lola Auring grew up by the ocean. She always admired the stars ever since she was young. Her family relies on fishing and catching other sea foods. She was able to get her family out of the poor life but her passion for the sea and stars remains. Now, together with her grandchildren she offers this activity by the sea.</p>"
        },

        "activity": [
            {
                "time": "6:00-7:00",
                "title": "Snacks",
                "detail": "Enjoy local snacks and delicacies."
            },
            {
                "time": "7:00-9:00",
                "title": "Catching Crabs at Night",
                "detail": "We'll catch crabs with the local fishermen."
            },
            {
                "time": "9:00-10:00",
                "title": "Bonfire and Dinner",
                "detail": "We'll cook our seafoods and go around a bonfire for dinner."
            },
            {
                "time": "10:00-12:00",
                "title": "Star Gazing",
                "detail": "We'll look at the stars and admire its beauty."
            },

        ],

        "about_place": "<p>The place is very homey and very nice go on a vacation. There are a lot of great place rest and the air quality is good. One will enjoy their stay at the bahay kubo near by. The dogs are also friendly</p>",

        "schedule": [
            {
                "date": "Wed, Sept 25 - 6:00 PM to 12:00 AM",
                "price": 200,
                "slots": "11 of 20"
            },
            {
                "date": "Thu, Sept 26 - 6:00 PM to 12:00 AM",
                "price": 200,
                "slots": "13 of 20"
            },
            {
                "date": "Sat, Sept 28 - 6:00 PM to 12:00 AM",
                "price": 200,
                "slots": "20 of 20"
            }
        ]

    }
}
