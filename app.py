from flask import Flask
from flask import render_template, send_from_directory
from data import DATA


class Config():
    APP_NAME = 'FarmBuild'

    # Enable debug mode
    DEBUG = True
    ALLOWED_HEADERS = ['Origin', 'Accept', 'Content-Type', 'X-Requested-With', 'X-CSRF-Token']
    ALLOWED_ORIGINS = '*'
    ALLOWED_METHODS = ['GET', 'HEAD', 'POST', 'OPTIONS', 'PUT', 'PATCH', 'DELETE']

    # TODO
    # This is where frontend should go, create a route for all UI files
    # Setup template folder for webpages
    TEMPLATE_FOLDER = "templates"


app = Flask(
    Config.APP_NAME, 
    template_folder=Config.TEMPLATE_FOLDER, 
    static_folder=Config.TEMPLATE_FOLDER
)

# Loads the needed config in the configuration file
app.config.from_object(Config)

# Handles CORS and other Cross Origin settings 
# Uses the after request decorator
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', app.config['ALLOWED_ORIGINS'])
    response.headers.add('Access-Control-Allow-Headers', ','.join(app.config['ALLOWED_HEADERS']))
    response.headers.add('Access-Control-Allow-Methods', ','.join(app.config['ALLOWED_METHODS']))
    return response

@app.route('/details/<listing_id>', methods=['GET'])
def get_details(listing_id):

    if listing_id not in DATA:
        return "error"

    info = DATA[listing_id]
    title = info['title']
    tags = info['tags']
    return render_template('template.html',**locals())

@app.route('/', methods=['GET'])
def index_page():

    arr = []
    three = []

    for key, val in DATA.items():
        three.append({
            'id': key,
            'title': val['title'],
            'desc': val['what_to_do'],
            'img': val['images'][0],
            'price': val['schedule'][0]['price']
        })

        if len(three) == 3:
            arr.append(three)
            three = []

    if len(three) > 0:
        arr.append(three)

    return render_template('index.html',**locals())

@app.route('/js/<path:path>')
def send_js(path):
    print(path)
    return send_from_directory('templates/js', path)

@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('templates/css', path)

@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('templates/img', path)

@app.route('/assets/<path:path>')
def assets(path):
    return send_from_directory('templates/assets', path)

    
app.run(host='0.0.0.0', use_reloader=True, threaded=True)
